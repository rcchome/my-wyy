import Vue from 'vue'
import App from './App'
// 引入uView主JS库
import uView from "uview-ui";
//引入vuex
import store from './store'
//把vuex定义成全局组件
Vue.prototype.$store = store

Vue.use(uView);


Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
