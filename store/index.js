import Vue from 'vue'
import Vuex from 'vuex'
import {
	getSong,
	getSongDetail
} from '@/api/index.js';
Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		id:'',
		src: '',
		imgUrl: '',
		songName: '',
		songAuthor: '', //作者
		songList: [], //歌单
		songListIndex: -1, //控制自动播放的歌曲
		//  audio 组件状态
		audio: null,
		isPlay: false,
		duration: 0,
		timer: null,
		playTime: 0,
		percent: 0,
		showProgress: true

	},
	mutations: {
		setId(state, val) {
			state.id = val
		},
		setSrc(state, val) {
			state.src = val
		},
		setSongList(state, val) {
			state.songList = val
		},
		setSongAuthor(state, val) {
			state.songAuthor = val
		},
		setSongListIndex(state, val = null) {
			let length = state.songList.length
			if (val > length) {
				state.songListIndex = 0
			} else {
				if (val !== null) {
					state.songListIndex = val
				} else {
					state.songListIndex++
					console.log('歌单索引+1',state.songListIndex);
				}
			}
		},
		setImgUrl(state, val) {
			state.imgUrl = val
		},
		setSongName(state, val) {
			state.songName = val
		},
		setAudio(state, val) {
			state.audio = val
		},
		setIsPlay(state, val) {
			state.isPlay = val
		},
		setDuration(state, val) {
			state.duration = val
		},
		setTimer(state, val) {
			state.timer = val
		},
		setPlayTime(state, val) {
			if(val===0){
				state.playTime=0
			}else{
				state.playTime++
			}
		},
		setPercent(state, val) {
			state.percent = val
		},
		setShowProgress(state, val) {
			state.showProgress = val
		},
		clearInterval(state,val){
			if(state.timer) {
				console.log('暂停计时');
				clearInterval(state.timer)
			} 
		}
	},
	actions: {
		getIsPlay({
			commit,
			state
		}){
			return new Promise((resolve, reject)=>{
				resolve(state.isPlay)
			})
		},
		startTime({
			commit,
			state
		}) {
			commit('setTimer',setInterval(() => {
				console.log('开始计时');
				commit('setPlayTime',state.playTime+1)
				commit('setPercent',(state.playTime / state.duration) * 100)
			}, 1000))
		},
		next({
			commit,
			state
		},type){
			return new Promise((resolve, reject) =>{
				commit('setPercent',0)
				let length = state.songList.length - 1
				console.log(length,'歌单长度');
				if (!length) return reject('歌单数量',length)
				
				if (type === 'next') {
					if (state.songListIndex < length) {
						commit('setSongListIndex')
					} else {
						commit('setSongListIndex',0)
					}
					resolve(state.songListIndex)
				} else {
					if (state.songListIndex !== 0 && state.songListIndex < length) {
						let index = state.songListIndex - 1
						commit('setSongListIndex',index)
					} else {
						let index = length
						commit('setSongListIndex',index)
					}
					resolve(state.songListIndex)
				}
			})
		},
		asyncGetSongDetails({
			commit,
			state
		}, id) {
			return new Promise(async (resolve, reject) => {
				try {

					// 获取 url
					const {
						data: res
					} = await getSong({
						id: id
					})
					if (res.data[0].url) {
						commit('setSrc', res.data[0].url)
						commit('setId', id)
						console.log(state.src,'src');
					} else {
						reject('无法播放')
						return false;
					}

					// 获取歌曲的 详情
					const {
						data: {
							songs
						}
					} = await getSongDetail({
						ids: id
					})
					commit('setImgUrl', songs[0].al.picUrl)
					commit('setSongName', songs[0].name)
					let singer = []
					songs[0].ar.forEach(item => {
						singer.push(item.name)
					})
					commit('setSongAuthor', singer.join(','))

					// 获取当前播放的歌曲有没有在 歌单中 
					if (state.songList.length > 0) {
						let index = state.songList.findIndex(item => {
							return item.id === state.id
						})
						console.log(index)
						commit('setSongListIndex', index)
					}
					resolve()
				} catch (e) {
					//TODO handle the exception
					reject(e)
				}
			})
		},
		
	}
})
export default store
