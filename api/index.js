import http from '@/utils/http.js'


export const getSwiperImg=(params)=>{
	return http({
		url:'/banner',
		method:'GET',
		params
	})
}
export const getPersonalized=(params)=>{
	return http({
		url:'/personalized',
		method:'GET',
		params
	})
}

//推荐新音乐
export const getPlaylist=(params)=>{
	return http({
		url:'/personalized/newsong',
		method:'GET',
		params
	})
}

//推荐视频
export const recommendVideo=(params)=>{
	return http({
		url:'/top/mv',
		method:'GET',
		params
	})
}
//获取音乐
export const getSong=(params)=>{
	return http({
		url:'/song/url',
		method:'GET',
		params
	})
}


//精品歌单标签列表
export const getPlaylistTags=(params)=>{
	return http({
		url:'/playlist/highquality/tags',
		method:'GET',
		params
	})
}
// 获取精品歌单
export const getPlaylistHighquality=(params)=>{
	return http({
		url:'/top/playlist/highquality',
		method:'GET',
		params
	})
}

//获取歌单详情
export const getPlaylistDetail=(params)=>{
	return http({
		url:'/playlist/detail',
		method:'GET',
		params
	})
}

//获取歌曲详情  //song/detail?ids=347230
export const getSongDetail=(params)=>{ 
	return http({
		url:'/song/detail',
		method:'GET',
		params
	})
}

//搜索
export const getSearch=(params)=>{ 
	return http({
		url:'/search',
		method:'GET',
		params
	})
}


//默认搜索关键词  
export const getSearchDefault=(params)=>{ 
	return http({
		url:'/search/default',
		method:'GET'
	})
}
//搜索建议
export const getSearchSuggest=(params)=>{ 
	return http({
		url:'/search/suggest',
		method:'GET',
		params
	})
}